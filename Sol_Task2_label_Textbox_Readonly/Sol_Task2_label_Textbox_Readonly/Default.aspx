﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_Task2_label_Textbox_Readonly.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

             <asp:ScriptManager ID="scriptManger" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>

                    <asp:Label ID="lblName1" runat="server" ReadOnly="true"></asp:Label><br />
                     
                    <asp:Label ID="lblName2" runat="server" ReadOnly="true"></asp:Label><br />
                    
                    <asp:Label ID="lblName3" runat="server" ReadOnly="true"></asp:Label><br>
                    
                    <asp:Button ID="btnEdit" runat="server" Text="Edit" postBackUrl="~/WebPage1.aspx" />
                   
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" PostBackUrl="~/Webpage2.aspx"  />
                     </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
