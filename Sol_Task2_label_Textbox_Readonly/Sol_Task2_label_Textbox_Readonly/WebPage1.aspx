﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebPage1.aspx.cs" Inherits="Sol_Task2_label_Textbox_Readonly.WebPage1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Textbox ID="FirstName" runat="server" ReadOnly="false"></asp:Textbox><br>
            <asp:Textbox ID="MiddleName" runat="server" ReadOnly="false"></asp:Textbox><br>
            <asp:Textbox ID="LastName" runat="server" ReadOnly="false"></asp:Textbox><br>
             <asp:Button ID="btnSubmit" runat="server" Text="Submit" PostBackUrl="~/Webpage2.aspx"  />
        </div>
    </form>
</body>
</html>
