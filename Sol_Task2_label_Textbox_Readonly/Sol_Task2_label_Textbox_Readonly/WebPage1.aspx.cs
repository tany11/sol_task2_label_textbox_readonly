﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Task2_label_Textbox_Readonly
{
    public partial class WebPage1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {



        }

       

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            
            FirstName.ReadOnly = false;
            MiddleName.ReadOnly = false;
            LastName.ReadOnly = false;

            if (Page.PreviousPage != null)
            {



                string strName1 =
                    ((Label)PreviousPage.FindControl("FirstName")).Text;
                string strName2 =
                    ((Label)PreviousPage.FindControl("MiddleName")).Text;
                string strName3 =
                    ((Label)PreviousPage.FindControl("LastName")).Text;

                FirstName.Text = strName1;
                MiddleName.Text = strName2;
                LastName.Text = strName3;

            }

           
        }
}
}